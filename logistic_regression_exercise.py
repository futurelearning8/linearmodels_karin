#My Model intercept: -2.570939302444458
#My Model slope: [-6.679398  -7.287445  -6.623838   0.1714808]
#My Model acc: 0.9846938775510204]

#Logistic regression intercept: [-1.56783449]
#Logistic regression slope:     [-4.92945753 -5.04162337 -4.61537039  0.23698511]
#Logistic regression Accuracy: 0.981

"""
The goal of this exercise is to get acquainted with the machine learning building blocks.
There are many methods and algorithms to train a machine learning model, but most of them share the same structure:
1. Load the data and preprocess it
2. Define the hypothesis and cost function
3. Design a learning algorithm (e.g. gradient descent)
4. Set the training hyperparameters (learning rate, number of iterations/epochs, etc.)
5. String all of the above to perform training
6. Evaluate your model and repeat (next week)
"""

# import the packages you plan on using
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score


def classify(predictions):
    return np.where(predictions > 0.5, 1, 0)


def sigmoid(z):
    sig = 1 / (1 + np.exp(-z))
    return sig


def hypothesis(X, theta):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :return H: (numpy array or scalar) Model's output on the sample set
    """
    h = sigmoid(X.dot(theta))
    return h


def cost(X, theta, y):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :param y: (numpy array) Target vector (ground truth for each sample)
    :return cost: (scalar) The model's parameters mean loss for all samples
    """
    m = X.shape[0]
    h = hypothesis(X, theta)
    J = - 1 / m * np.sum(y * np.log(h) + (1-y) * np.log(1 - h))
    return J


def gradient(X, theta, y):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :param y: (numpy array) Target vector (ground truth for each sample)
    :return gradient: (numpy array or scalar) parameter gradients for current step
    """
    m = X.shape[0]
    grad = 1 / m * (hypothesis(X, theta) - y).dot(X)
    return grad


def batch_gradient_descent(X, y, theta, lr, batch_size, num_epochs):
    """
    :param X: (numpy array) rows: examples ,  cols: features
    :param y: (numpy array) target vector
    :param theta: (numpy array) model parameters
    :param lr: (float) learning rate
    :param batch_size: (int) training batch size per iteration
    :param num_epochs: (int) total number of passes through the data
    :return theta: (numpy array) optimized model parameters
    """

    m = X.shape[0]
    steps_per_epoch = int(m / batch_size) + 1
    # start a loop over the number of epochs
    for epoch in range(num_epochs):
        for step in range(steps_per_epoch):
            X_batch = X[step * batch_size: step * batch_size + batch_size]
            y_batch = y[step * batch_size: step * batch_size + batch_size]
            # compute the gradient for this batch
            grad = gradient(X_batch, theta, y_batch)
            # update the model parameters according to learning rate
            theta -= lr * grad
        loss = cost(X, theta, y)
        print("Epoch {} loss {}".format(epoch, loss))
    # return the optimized model parameters
    return theta


def main():
    # load the WW2 weather data
    data_path = "data/data_banknote_authentication.csv"
    df = pd.read_csv(data_path)

    y_column = ['authenticity']
    features = ['variance', 'skewness', 'curtosis', 'entropy']

    # drop none values
    df = df.dropna(subset=features+y_column)

    # shuffle the data
    df = df.sample(frac=1)

    # add bias column to data
    df.insert(0, 'bias', 1)
    features.insert(0, 'bias')

    # prepare it for training (grab only relevant columns, split to X (samples) and y (target)
    y = df.loc[:, y_column[0]]
    X = df.loc[:, features]

    # normalize the data
    X_to_normalize = X[features[1:]]
    X[features[1:]] = (X_to_normalize - X_to_normalize.mean()) / X_to_normalize.std()

    X_without_bias = X.loc[:, features[1:]].to_numpy()
    X = X.to_numpy()

    # initialize theta (model parameters)
    n = X.shape[1]
    theta = np.zeros(n).astype('float32')

    # configure training hyperparameters
    lr = 0.5
    batch_size = 500
    num_epochs = 1000

    # start training and return optimal parameters
    theta = batch_gradient_descent(X, y, theta, lr, batch_size, num_epochs)
    y_hat = hypothesis(X, theta)
    print(f'\nMy Model cost: {cost(X, theta, y)}')
    print(f'My Model intercept: {theta[0]}')
    print(f'My Model slope: {theta[1:]}')
    y_hat = classify(y_hat)
    acc = accuracy_score(y, y_hat)
    print(f'My Model acc: {acc}]\n')

    # Sklearn Logistic Regression
    model = LogisticRegression(fit_intercept=True)
    model.fit(X_without_bias, y)
    print("Logistic regression intercept:", model.intercept_)
    print("Logistic regression slope:    ", model.coef_[0])
    y_m = model.predict(X_without_bias)
    # evaluate the predictions
    acc = accuracy_score(y, y_m)
    print('Logistic regression Accuracy: %.3f' % acc)


if __name__ == '__main__':
    main()
