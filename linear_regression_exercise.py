#My Model intercept: 0.000277143029961735
#My Model slope: [0.8797848]
#My Model Coefficient of determination: 0.7715562468555665

#Model intercept: -8.385280784446702e-16
#Model slope:     [0.87838391]
#Model Coefficient of determination: 0.7715582862315505

"""
The goal of this exercise is to get acquainted with the machine learning building blocks.
There are many methods and algorithms to train a machine learning model, but most of them share the same structure:
1. Load the data and preprocess it
2. Define the hypothesis and cost function
3. Design a learning algorithm (e.g. gradient descent)
4. Set the training hyperparameters (learning rate, number of iterations/epochs, etc.)
5. String all of the above to perform training
6. Evaluate your model and repeat (next week)
"""

# import the packages you plan on using
import pandas as pd
import numpy as np
from sklearn.linear_model import SGDRegressor, LinearRegression
import matplotlib.pyplot as plt
from sklearn.metrics import r2_score


def hypothesis(X, theta):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :return H: (numpy array or scalar) Model's output on the sample set
    """
    h = X.dot(theta)
    return h


def cost(X, theta, y):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :param y: (numpy array) Target vector (ground truth for each sample)
    :return cost: (scalar) The model's parameters mean loss for all samples
    """
    m = X.shape[0]
    h = hypothesis(X, theta)
    J = 1 / (2 * m) * np.sum((h - y)**2)
    return J


def gradient(X, theta, y):
    """
    :param X: (numpy array) Set of samples you apply the model on
    :param theta: (numpy array) Model parameters
    :param y: (numpy array) Target vector (ground truth for each sample)
    :return gradient: (numpy array or scalar) parameter gradients for current step
    """
    m = X.shape[0]
    grad = 1 / m * (hypothesis(X, theta) - y).dot(X)
    return grad


def batch_gradient_descent(X, y, theta, lr, batch_size, num_epochs):
    """
    :param X: (numpy array) rows: examples ,  cols: features
    :param y: (numpy array) target vector
    :param theta: (numpy array) model parameters
    :param lr: (float) learning rate
    :param batch_size: (int) training batch size per iteration
    :param num_epochs: (int) total number of passes through the data
    :return theta: (numpy array) optimized model parameters
    """

    m = X.shape[0]
    steps_per_epoch = int(m / batch_size) + 1
    # start a loop over the number of epochs
    for epoch in range(num_epochs):
        for step in range(steps_per_epoch):
            X_batch = X[step * batch_size: step * batch_size + batch_size]
            y_batch = y[step * batch_size: step * batch_size + batch_size]
            # compute the gradient for this batch
            grad = gradient(X_batch, theta, y_batch)
            # update the model parameters according to learning rate
            theta -= lr * grad
        loss = cost(X, theta, y)
        print("Epoch {} loss {}".format(epoch, loss))
    # return the optimized model parameters
    return theta


def main():
    # load the WW2 weather data
    data_path = "data/weatherww2.csv"
    df = pd.read_csv(data_path)

    y_column = ['MaxTemp']
    features = ['MinTemp']
    # drop none values
    df = df.dropna(subset=features+y_column)
    print(f"Num of examples: {df.shape[0]}\n")

    # shuffle the data
    df = df.sample(frac=1)

    # add bias column to data
    df.insert(0, 'bias', 1)
    features.insert(0, 'bias')

    # prepare it for training (grab only relevant columns, split to X (samples) and y (target)
    y = df.loc[:, y_column[0]]
    X = df.loc[:, features]

    # normalize the data
    X_to_normalize = X[features[1:]]
    X[features[1:]] = (X_to_normalize - X_to_normalize.mean()) / X_to_normalize.std()
    y = (y - y.mean()) / y.std()

    X_without_bias = X.loc[:, features[1:]].to_numpy()
    X = X.to_numpy()

    # initialize theta (model parameters)
    n = X.shape[1]
    theta = np.zeros(n).astype('float32')

    # configure training hyperparameters - change according data - is configures for MinTemp-MaxTemp
    lr = 0.2
    batch_size = 50000
    num_epochs = 100

    # start training and return optimal parameters
    theta = batch_gradient_descent(X, y, theta, lr, batch_size, num_epochs)
    y_hat = hypothesis(X, theta)
    print(f'\nMy Model cost: {cost(X, theta, y)}')
    print(f'My Model intercept: {theta[0]}')
    print(f'My Model slope: {theta[1:]}')
    print(f'My Model Coefficient of determination: {r2_score(y, y_hat)}\n')

    # Sklearn Linear Regression
    model = LinearRegression(fit_intercept=True)
    model.fit(X_without_bias, y)
    y_hat_lr = model.predict(X_without_bias)
    print("Model intercept:", model.intercept_)
    print("Model slope:    ", model.coef_)
    print("Model Coefficient of determination:", model.score(X_without_bias, y))

    # plot
    if len(features) - 1 == 1:
        plt.scatter(X_without_bias, y)
        plt.plot(X_without_bias, y_hat, 'm+', label='My Model')
        plt.plot(X_without_bias, y_hat_lr, 'y+', label='Linear Regression')
        leg = plt.legend()
        plt.show()


if __name__ == '__main__':
    main()
